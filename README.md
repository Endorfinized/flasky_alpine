# README #
Insight Engines DevOps challenge.  
Have been given this challenge to build flask on alpine on port 9k    

### What is this repository for? ###
* For showing my future co-workers I have a sense of what I'm doing
* Version marked latest


### How do I get set up? ###
I loaded up this docker image with a few tools.  
As well as upgrade for pip's  
python3 
py-pip  
curl   
bash  
uwsgi-python  
uwsgi  

* dockerfile and requirments.txt are in /app
* To get your image

```fenced-style
docker pull docker4mmclinn/flasky_alpine
```
* To run it
```fenced-style
docker run -p 9000:9000 IMAGE_ID
```
* If you need to run additional python packages
```fenced-style
docker exec Container_ID/Name /bin/bash -c "pip install package-name"
```
* check to make sure your port is open  Your response     
```fenced-style
"<h2 style='color:green'>Welcome to Insight Engines</h2>" 
curl -X GET 127.0.0.1:9000
```
```fenced-style
curl -X GET 127.0.0.1:9000
```
* or if you're running something that blocks your loopback. You should have same response as above.
```fenced-style
curl -X GET "http://[::1]:9000"
```

### Who do I talk to? ###
* There is only Zule

### Docker Repo ###

```fenced-style
docker pull docker4mmclinn/flasky_alpine
```
* Link
https://hub.docker.com/r/docker4mmclinn/flasky_alpine/

### Issues ####

* Attempted to build using pipeline.yml and dockerfile at same time.
* Docker Desktop issues with proxy and firewall. ~Resolved
* Bad CMD pulled all my free pipeline time ditched yml dev. Created new repo for more time to build yml 
* Docker build pushed to repo- Resolved with docker proxy trust
* Couldn't read requirements.txt file till adding - resolved trusted host
```fenced-style
--trusted-host pypi.python.org 
```
* baffled by localhost:port in use... went for IPV6 Thanks rfc2732
```fenced-style
http://[::]:9000/
```
* success ; curling on ipv6
* curl -g [::]:9000
* discovered A/V software silent block 127.0.0.1
* discovered defacto block in MACOS(systems) and host 
* discovered ipv6 loopback will resove for ipv4 as well. yay

