from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return "<h2 style='color:green'>Welcome to Insight Engines</h2>"

if __name__ == '__main__':
    # app.run()
    app.run(host='::' , debug=True, port=9000)